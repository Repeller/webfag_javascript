import axios from 'Axios';

// alle disse eksempler skal bruges inde i consolen på chrome browseren
// med undtagelse af 5 til 7

// -------------------------------------
console.log('Opgaver – Promise (Ajax, fetch, Axios, await/async)'); 
// Done

var promise = new Promise((resolved, rejected) =>{
    resolved();
    rejected();
});

promise
    .then(() => console.log('it works'))
    .catch(() => console.log('you fucked up'));

// here is the code
promise = new Promise((resolve, reject)  => {
    resolve(); 
    reject();
});

console.log('wait for it');
setTimeout(() => 
{promise
    .then(()=>console.log('Im finish'))          //callback, udføres ved resolved
    .catch(() => console.log('uh oh!!'))}, 8000);         //callback, udføres ved reject

// -------------------------------------
console.log('Opgave (Promise – 2, Ajax)'); 
// 

promise = new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
  
    request.open('GET', 'https://api.icndb.com/jokes/random');
    request.onload = () => {
      if (request.status === 200) {
        resolve(request.response); // we got data here, so resolve the Promise
      } else {
        reject(Error(request.statusText)); // status is not 200 OK, so reject
      }
    };
  
    request.onerror = () => {
      reject(Error('Error fetching data.')); // error occurred, reject the  Promise
    };
  
    request.send(); // send the request
  });
  
  console.log('Asynchronous request made.');
  
  promise.then((data) => {
    console.log('Got data! Promise fulfilled.');
    document.body.textContent = JSON.parse(data).value.joke;
  }, (error) => {
    console.log('Promise rejected.');
    console.log(error.message);
  });
  

// -------------------------------------
console.log('Opgave (Promise – 3, fetch)'); 
// 1
//url = "https://jsonplaceholder.typicode.com/posts/"; // all fine
// 2
//url = "https://jsonplaceholder.typicode.com/posts12345/"; // error and empty array
// 3
url = "https://jsonplaceholder.typicode.dk/posts/"; // never gets something back

// fetch(url)
//   .then(data => console.log(data));

fetch(url)
  .then(responce => responce.json())
  .then(data => console.log(data));


// -------------------------------------
console.log('Opgave (Promise – 4, fetch)'); 
// 
url4 = "https://ergast.com/api/f1/2018/drivers.json"; // never gets something back

fetch(url4)
  .then(responce => responce.json())
  .then(data => console.log(data));

/*
MRData: {xmlns: "http://ergast.com/mrd/1.4", 
        series: "f1", 
        url: "http://ergast.com/api/f1/2018/drivers.json", 
        limit: "30", 
        offset: "0", …}
__proto__: Object
*/

// -------------------------------------
console.log('Opgave (Promise – 5, Ajax)'); 
// 
axios.get("https://ergast.com/api/f1/2018/drivers.json")
    .then((response) => {
        let data = response.data;

        data.forEach(element => {
            console.log(element);
        });
    })
    .catch((error) => {
        console.log(error);
        ErrorHandler.innerHTML = error.message;
    });

// -------------------------------------
console.log('Opgave (Promise – 6, fetch)'); 
// 


// -------------------------------------
console.log('Opgave (Promise – 7, fetch)'); 
// 