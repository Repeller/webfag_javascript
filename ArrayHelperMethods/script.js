// good links (this was added after I was done)

// javascript Array https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array


// every https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every
// filter https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
// map https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
// reduce https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
// push https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push
// some https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some

// sort https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
// reverse https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse

// -----------------------------------------------
// opgave (foreach - 1) - OO
console.log('opgave (foreach - 1)');

let names = ['HenriK', 'JAMshid', 'AndERS', 'EBBe', 'pER', 'MicHAel', 'PETEr'];

console.log('names before: ', JSON.parse(JSON.stringify((names))));

names.forEach((name, index_1, array) =>{
    
    let upercase = false;
    
    if(name == 'HenriK' || name == 'pER')
        upercase = true;

    if(upercase)
    {
        array[index_1] = name.toUpperCase();
    }
    else
    {
        array[index_1] = name.toLowerCase();
    }
});

// console.log('names after: ', JSON.parse(JSON.stringify((names))));

// -----------------------------------------------
// opgave (foreach - 2)
console.log('opgave (foreach - 2)');

let cars = [
    {brand: 'VW', model: 'Passat', fuel: 'diesel', owner_tax: 5550 },
    {brand: 'VW', model: 'Passat', fuel: 'gasoline', owner_tax: 460},
    {brand: 'VW', model: 'Passat', fuel: 'hybrid', owner_tax: 150},
    {brand: 'BMW', model: '320i', fuel: 'diesel', owner_tax: 4280},
    {brand: 'BMW', model: '320i', fuel: 'gasoline', owner_tax: 430},
    {brand: 'BMW', model: '320i', fuel: 'hybrid', owner_tax: 210},
    {brand: 'Tesla', model: 'S', fuel: 'electric', owner_tax: 0 }
];

console.log('cars before: ', JSON.parse(JSON.stringify((cars))));

var increaseOwnerTax = function(cars, fuel, taxPct){
    cars.forEach((car, index, array) => {
        
        if(car.fuel == fuel)
        {
            let test = car.owner_tax * (1.0 + (taxPct / 100));
            car.owner_tax = test;
            array[index] = car;
        }
    });
};

increaseOwnerTax(cars, 'diesel', 50);
increaseOwnerTax(cars, 'hybrid', 5);

console.log('cars after: ', JSON.parse(JSON.stringify((cars))));


// -----------------------------------------------
// opgave (map - 1) (A, B) - O
console.log('opgave (map - 1) (A, B)');

const map1 = cars.map(x => {
    return x.model;
});
const map2 = cars.map(x => {
    return x.brand;
});

console.log('map1: ', JSON.parse(JSON.stringify((map1))));
console.log('map2: ', JSON.parse(JSON.stringify((map2))));


// -----------------------------------------------
// opgave (map - 2) - O
console.log('opgave (map - 2)');

// Data, fra: http://ergast.com/api/f1/2018/drivers.JSON?callback=drivers 
var drivers = `[
    {
    "driverId": "grosjean",
    "permanentNumber": "8",
    "code": "GRO",
    "url": "http://en.wikipedia.org/wiki/Romain_Grosjean",
    "givenName": "Romain",
    "familyName": "Grosjean",
    "dateOfBirth": "1986-04-17",
    "nationality": "French"
    },
    {
    "driverId": "hamilton",
    "permanentNumber": "44",
    "code": "HAM",
    "url": "http://en.wikipedia.org/wiki/Lewis_Hamilton",
    "givenName": "Lewis",
    "familyName": "Hamilton",
    "dateOfBirth": "1985-01-07",
    "nationality": "British"
    },
    {
    "driverId": "hulkenberg",
    "permanentNumber": "27",
    "code": "HUL",
    "url": "http://en.wikipedia.org/wiki/Nico_H%C3%BClkenberg",
    "givenName": "Nico",
    "familyName": "Hülkenberg",
    "dateOfBirth": "1987-08-19",
    "nationality": "German"
    },
    {
    "driverId": "kevin_magnussen",
    "permanentNumber": "20",
    "code": "MAG",
    "url": "http://en.wikipedia.org/wiki/Kevin_Magnussen",
    "givenName": "Kevin",
    "familyName": "Magnussen",
    "dateOfBirth": "1992-10-05",
    "nationality": "Danish"
    }    
]`;

let f1Drivers = JSON.parse(drivers);
console.log('drivers: ', JSON.parse(JSON.stringify((f1Drivers))));

const myDrivers = f1Drivers.map(x => {
    var test = {};
    test.kode = x.code;
    test.fornavn = x.givenName;
    test.efternavn = x.familyName;
    
    // both of this works
    // test.test = x.code;
    // test['kode'] = x.code;

    return test;
});

console.log('myDrivers: ', JSON.parse(JSON.stringify((myDrivers))));

// -----------------------------------------------
// opgave (filter - 1) - O
console.log('opgave (filter - 1)');
var greenCars = cars.filter(car => {
    if(car.fuel == 'hybrid' && car.owner_tax < 200)
    {
        return car;
    }
});
console.log('car filter:', JSON.parse(JSON.stringify((greenCars))));


// -----------------------------------------------
// opgave (filter - 2) - O
console.log('opgave (filter - 2)');

function fuelCriterium(car5, fuel5)
{
    if(car5.fuel == fuel5)
    {
        return true;
    }
    else
    {
        return false;
    }
};

var gasCars = cars.filter(car => {
    if(fuelCriterium(car, 'gasoline'))
    {
        return car;
    }
});

console.log('gas cars:', JSON.parse(JSON.stringify((gasCars))));

// -----------------------------------------------
// opgave (reduce - 1) - 0
console.log('opgave (reduce - 1)');
var trips = [{distance : 48}, {distance : 12}, {distance : 6}];

const totalDistance = trips.reduce((acc, x) => acc + x.distance, 0);

console.log('total:', JSON.parse(JSON.stringify((totalDistance))));

// -----------------------------------------------
// opgave (reduce - 2) - 0
console.log('opgave (reduce - 2)');

var desk = [
    {type : 'sitting'}, 
    {type : 'standing'}, 
    {type : 'sitting'}, 
    {type : 'sitting'},
    {type : 'standing'}
];

var test1 = desk.reduce((acc, desk) => {
    if(desk.type == 'sitting')
    {
        acc.sitting++;
    }
    if(desk.type == 'standing')
    {
        acc.standing++;
    }
    return acc;
}, {sitting : 0, standing : 0} );
console.log('test', JSON.parse(JSON.stringify((test1))));

//console.log('desktypes:', JSON.parse(JSON.stringify((desktypes))));


// -----------------------------------------------
// opgave (reduce/find - 3) - O
console.log('opgave (reduce/find - 3)');
var numbers = [1, 1, 2, 3, 4, 4];

// const test2 = numbers.find(x => x == 7);
// // this is undefined
// console.log('test2',test2);

function unique(arrayObj) {
    var uniqueNums = arrayObj.reduce((acc, num) => {

        // at the current 'num' in 'arrayObj'
        // check if the number is not in the 'acc' array
        if(typeof (acc.find(element => element == num)) === 'undefined')
        {
            acc.push(num);
        }
        return acc;

    }, [] );
    return uniqueNums;
};

console.log('test3', unique(numbers));

// -----------------------------------------------
// opgave (reduce - 3) - O
console.log('opgave (reduce - 3)');

var inputTest1 = '))()((';
var inputTest2 = '(())';
var inputTest3 = '((((((()';


function balanceParens(inputValue){
    // convert to array
    var inputArray = Array.from(inputValue);
    
    // reduce it
    var output = inputArray.reduce((acc, symbol) => {
        // if there are more closers than opens, then it don't work
        if(acc.counter < 0)
        {
            acc.blanced = false;
        }
        
        if(symbol == '(' )
        {
            acc.counter++;
        }
        if(symbol == ')' )
        {
            acc.counter--;
        }
        return acc;
    }, {counter : 0, blanced : true} );

    // just in case, that there are opens in the start, but not an equal amount of closers
    if(output.counter > 0)
    {
        output.blanced = false;
    }

    return output.blanced;
};

console.log('inputTest1',balanceParens(inputTest1));
console.log('inputTest2',balanceParens(inputTest2));
console.log('inputTest3',balanceParens(inputTest3));


// -----------------------------------------------
// opgave (filter, map og reduce - 1) - O
console.log('opgave (filter, map og reduce - 1)');

var personnel = [
    {
      id: 5,
      name: "Luke Skywalker",
      pilotingScore: 98,
      shootingScore: 56,
      isForceUser: true,
    },
    {
      id: 82,
      name: "Sabine Wren",
      pilotingScore: 73,
      shootingScore: 99,
      isForceUser: false,
    },
    {
      id: 22,
      name: "Zeb Orellios",
      pilotingScore: 20,
      shootingScore: 59,
      isForceUser: false,
    },
    {
      id: 15,
      name: "Ezra Bridger",
      pilotingScore: 43,
      shootingScore: 67,
      isForceUser: true,
    },
    {
      id: 11,
      name: "Caleb Dume",
      pilotingScore: 71,
      shootingScore: 85,
      isForceUser: true,
    },
  ];

  // filter only the things we need
  var perOpgave1 = personnel.filter(x => x.shootingScore > 60 && x.isForceUser == true );
  // remove everything but what we need
  var perOpgave2 = perOpgave1.map(x => x.shootingScore);
  // count up the total we have
  var perOpgave3 = perOpgave2.reduce((acc, x) => acc + x, 0);
  console.log('perOpgave1', perOpgave1);
  console.log('perOpgave2', perOpgave2);
  console.log('perOpgave3', perOpgave3);



// -----------------------------------------------
// opgave (every - 1) - O 
console.log('opgave (every - 1)');
var overOrAt20 = personnel.every(x => x.pilotingScore >=20 );
console.log('har alle over / lig med 20 piloting score:', overOrAt20);

// -----------------------------------------------
// opgave (some - 1) - O
console.log('opgave (some - 1)');
var overOrAt80Force = personnel.some(x => x.shootingScore >= 80 && x.isForceUser == false);

console.log('har bare en score på 80 eller over, som ikke bruger force?', overOrAt80Force);

// -----------------------------------------------
// opgave (ekstra) - O
console.log('opgave (ekstra)');

var players = personnel.map(x => x.name);
console.log('normal order', JSON.parse(JSON.stringify((players))));

players.sort();
console.log('now in a sorted order', JSON.parse(JSON.stringify((players))));

players.reverse();
console.log('reverse order', JSON.parse(JSON.stringify((players))));
