// -------------------------------------
console.log('Opgave (Closures - 1)'); 
// done

function buildFunctions(){
    var arr=[];
    
    // var i , was the problem, since it is seen as a global variable
    // we should use let, since that is seen as a local block scrope variable
    
    // since we used var, there will only be used one variable in memory
    // and the execute happens before they get printed in the console.
    
    for (let i=0; i<3; i++){
        arr.push(function() {console.log(i);})
    }
    return arr;
}

var fs = buildFunctions();
fs[0]();
fs[1]();
fs[2]();
// I think 0, 1, 2

// but I get 3, 3, 3

// after changing the 'var i' to 'let i'
// we get 0, 1, 2


// -------------------------------------
console.log('Opgave (Closures - 2, function factories)');
// done
function makeGreeting(language) {
    // new way
    if (language==='en')
    {
        return (firstname, lastname) => {console.log('Hello ' + firstname + ' ' + lastname)} ;
    }
    if (language==='dk')
    {
        return (firstname, lastname) => {console.log('Hej ' + firstname + ' ' + lastname)} ;
    }
    if (language==='es')
    {
        return (firstname, lastname) => {console.log('Hola ' + firstname + ' ' + lastname)} ;
    }
    
    // old way
    // return function(firstname, lastname){
    //     if (language==='en'){console.log('Hello ' + firstname + ' ' + lastname)};
    //     if (language==='dk'){console.log('Hej ' + firstname + ' ' + lastname)};
    //     if (language==='es'){console.log('Hola ' + firstname + ' ' + lastname)}
    // }
}

// here we asign a returning function object to a variable
// we have given the parameter of (language) in the function of makeGreeting
var greetEnglish = makeGreeting('en');
var greetDanish = makeGreeting('dk');
var greetSpanish = makeGreeting('es');

// if we want to call the function, we will haft to give the 2 parameers 'firsname' and 'lastname'
greetEnglish('John', 'Doe');
greetDanish('Henrik', 'Høltzer');
greetSpanish('Pablo', 'Fuentes');
// this all works, because the 'language' parameter allways is sat as a 'let' type. 
// therefore it stays in memory for when we need it later.
// that is the core idea behind 'closures'
// the values will stay forever in memory, since they are used in a function
// that is global.


// I think it will be normal
// and it came out normal

// -------------------------------------
console.log('Opgave (Closures – 3, function factories)');
// done

function makeAdder(x) {
    return (addedValue) => {
        return x + addedValue;
    };
}

var add5 = makeAdder(5);
var add10 = makeAdder(10);
var addHello = makeAdder("Hello ");

console.log(add5(2));  // 7
console.log(add10(2)); // 12
console.log(addHello("Henrik")); // Hello Henrik

// -------------------------------------
console.log('Opgave (Callback function)');
// done?

function tellMeWhenDone(callback){
    // ... some work
    let counter = String(callback).length;
    setTimeout(() => console.log('the callback|', callback, '| is', counter, 'characters long'), 0);
    // call the 'callback'
    callback();
}

tellMeWhenDone(function(){console.log("Im' done!")});
tellMeWhenDone(function(){console.log("All done!")});

tellMeWhenDone((x = 5,y = 5,z = 5) => {
    console.log([x, y, z]);
    console.log('x',x);
    console.log('y',y);
    console.log('z',z);
});